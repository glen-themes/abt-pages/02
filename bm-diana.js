$(document).ready(function(){
    var getfade = $("[fade-in]").attr("fade-in");
    if(getfade == "yes"){
        var get_bg = $("#background"),
            bg = get_bg.css("background-image");

        if (bg) {
            var src = bg.replace(/(^url\()|(\)$|[\"\'])/g, ''),
                $img = $("<img>").attr("src", src).on("load", function(){
                            get_bg.fadeIn();
                       });
        }
    } else {
        $("#background").show();
    }
    
    $("h1").each(function(){
        if($(this).find("a").length !== 0){
            $(this).addClass("rm_pd");
        }
    });
});// end ready
